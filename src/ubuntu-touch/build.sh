#!/bin/bash -e

SOURCES="$(dirname "$(readlink -f "${0}")")/../.."

DATE=$(date +%Y%m%d.%H%M)
ARCH=$(dpkg-architecture -qDEB_HOST_ARCH)
CLICK_TARGET_DIR="$SOURCES/build/tmp"

mkdir $CLICK_TARGET_DIR

echo "I: Building application"
mkdir build-${ARCH}; cd build-${ARCH}
cmake -GNinja ../.. -DCMAKE_INSTALL_PREFIX=/
DESTDIR=$CLICK_TARGET_DIR ninja install

echo "I: Copying application"
cp -r \
	$SOURCES/src/ubuntu-touch/apparmor.json \
	$SOURCES/src/ubuntu-touch/run.sh \
	$SOURCES/src/ubuntu-touch/org.kde.kirigami2.gallery.desktop \
	$SOURCES/src/ubuntu-touch/kirigami.png \
	$CLICK_TARGET_DIR

sed "s/@CLICK_ARCH@/$ARCH/g;s/@CLICK_DATE@/$DATE/g" $SOURCES/src/ubuntu-touch/manifest.json.in > $CLICK_TARGET_DIR/manifest.json
